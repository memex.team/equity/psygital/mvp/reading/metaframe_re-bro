#######    ########    #######    ########    #######    #######    #######
#       / / / /    License    \ \ \ \ 
#     Copyright © 2020-2021 Memex.Team 
#     This code is covered by a Memex.Team Licensing policy (License.md) 
#     Feel free to use a power of Memex.Team & Memex.Team trademarks!
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#      Language = ruby
#      Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

FROM opensuse/leap:latest

RUN zypper --non-interactive --gpg-auto-import-keys up --no-recommends --allow-vendor-change --auto-agree-with-licenses \
    && zypper ar -cf https://download.opensuse.org/repositories/devel:/languages:/ruby/openSUSE_Leap_15.2/devel:languages:ruby.repo \
    && zypper --non-interactive --gpg-auto-import-keys in --no-recommends --allow-vendor-change --auto-agree-with-licenses \
      wget curl ca-certificates openssl ruby3.0 ruby3.0-devel git gcc gcc-c++ make libffi-devel zlib zlib-devel-static libxml2-devel tar gzip 
    
RUN echo "fs.inotify.max_user_watches=524288" > /etc/sysctl.d/max_user_watches-change.conf \
  && sysctl -p \
  && ulimit -n 64000
 
RUN git clone https://gitlab.com/memex.team/equity/archestry/digital/mediation/re-bro.git /app/
 
RUN mkdir -p /app/microservices/contextme/reading/metaframe
COPY . /app/microservices/contextme/reading/metaframe
WORKDIR /app

RUN bundle.ruby3.0 install --with=parsers elastic graph
RUN sed -i 's#mount(app.mount_instance.*#mount\(\{ app.mount_instance\(configuration: opts\[:with\] \|\| \{\}\) => path \}\)#' /usr/lib64/ruby/gems/3.0.0/gems/grape-1.5.1/lib/grape/dsl/routing.rb
ENV LD_PRELOAD=/usr/lib64/libjemalloc.so.2
CMD bundle.ruby3.0 exec agoo -t 20 -w 10 -p 2000 --log-console
