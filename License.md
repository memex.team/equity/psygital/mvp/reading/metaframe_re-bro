### Memex.Team Licensing policy
     Copyright © 2020-2021 Memex.Team 
     A vision, a concepts, an architecture, an approaches, 
     a metamodels are protected by copyrights & trademarks.

     All texts/code as a realization of a vision, a concepts, 
     an architecture, an approaches & a metamodels are protected by 
     Creative Commons Attribution-ShareAlike 4.0 License (CC BY-SA 4.0)
     Refer to the http://creativecommons.org/licenses/by-sa/4.0/
     Feel free to use a power of Memex.Team & Memex.Team trademarks!


### How to use     
Please add reference to a "Memex.Team Licensing policy" in all text/source file headers in a proper way:

#### 1) Short header (preferable):
```
#######    ########    #######    ########    #######    #######    #######
#       / / / /    License    \ \ \ \ 
#     Copyright © 2020-2021 Memex.Team 
#     Copyright © 2020-2021 {AUTHORS}
#     This code is covered by a Memex.Team Licensing policy (License.md) 
#     Feel free to use a power of Memex.Team & Memex.Team trademarks!
#######    ########    #######    ########    #######    #######    #######
```

#### 2) Full header:

```
#######    ########    #######    ########    #######    #######    #######
#       / / / /    License    \ \ \ \ 
#     Copyright © 2020-2021 Memex.Team 
#     Copyright © 2020-2021 {AUTHORS}
#     A vision, a concepts, an architecture, an approaches, 
#     a metamodels are protected by copyrights & trademarks.
#
#     All texts/code as a realization of a vision, a concepts, 
#     an architecture, an approaches & a metamodels are protected by 
#     Creative Commons Attribution-ShareAlike 4.0 License (CC BY-SA 4.0)
#     Refer to the http://creativecommons.org/licenses/by-sa/4.0/
#     Feel free to use a power of Memex.Team & Memex.Team trademarks!
#######    ########    #######    ########    #######    #######    #######
```
