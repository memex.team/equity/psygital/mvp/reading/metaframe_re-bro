#######    ########    #######    ########    #######    #######    #######
#       / / / /    License    \ \ \ \ 
#     Copyright © 2020-2021 Memex.Team 
#     This code is covered by a Memex.Team Licensing policy (License.md) 
#     Feel free to use a power of Memex.Team & Memex.Team trademarks!
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#      Language = ruby
#      Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######
# frozen_string_literal: true
Bundler.require(:parsers, :elastic, :graph)
require 'elasticsearch/transport'
require 'elasticsearch/dsl'
require 'neo4j_ruby_driver'
require 'active_graph'

module ReBro
  module Env
    class << self
      
      def reBroMicroservice
        res = {}
        name = ENV.fetch('ReBroMicroservice').split('__')
        res[:fqnC] = name[0]
        res[:fqnO] = name[1]
        res[:fqnF] = name[2]   
        res[:class] = "ReBro::Microservices::#{res[:fqnC].capitalize}::#{res[:fqnO].capitalize}::#{res[:fqnF].capitalize}"        
        res
      end
      
      
      def logLevel
        res = {}
        res[:dev] = Logger::DEBUG
        res[:prod] = Logger::INFO
        res
      end
      
      
      def api
        naming = reBroMicroservice
        res = {} 
        res[:path] = '/api/contextme/reading/metaframe'
        res[:class] = "ReBro::Microservices::#{naming[:fqnC].capitalize}::#{naming[:fqnO].capitalize}::#{naming[:fqnF].capitalize}::API".constantize
        res
      end   
      
      def microservicePath
        path = "#{File.dirname(File.expand_path(__FILE__))}"
        path
      end   
      
      def apm
        res = {} 
        res[:server_url] = ENV.fetch('ApmURL')
        res[:secret_token] = ENV.fetch('ApmToken')
        res[:service_name] = ENV.fetch('ReBroMicroservice')
        res
      end  
      
      def neo4j
        res = {} 
        res[:url] = ENV.fetch('Neo4jURL')
        res[:user] = ENV.fetch('Neo4jUser')
        res[:password] = ENV.fetch('Neo4jPassword')
        res
      end
      
      ActiveGraph::Base.driver = Neo4j::Driver::GraphDatabase.driver(ReBro::Env.neo4j[:url], Neo4j::Driver::AuthTokens.basic(ReBro::Env.neo4j[:user], ReBro::Env.neo4j[:password]), encryption: false) 
            
      def eventflow
        res = {}
        res[:url] = ENV.fetch('EventflowURL')
        res[:token] = ENV.fetch('EventflowToken')
        res
      end     
      
      def elasticsearch
        res = {}        
        res[:host] = ENV.fetch('ESHost', 'localhost')
        res[:port] = ENV.fetch('ESPort')
        res[:user] = ENV.fetch('ESUser')
        res[:pass] = ENV.fetch('ESPass')
        res
      end
      
      def auth
        res = jsonFile2Mash(currRbFile: __FILE__, jsonFile: "#{corePathString}/_env/#{ENV.fetch('Auth')}")
        res
      end      
      
    end
    
    module Mixin
      module Auth
        @@authDB = ReBro::Env.auth
      end
    end    
    
  end
end
