#######    ########    #######    ########    #######    #######    #######
#       / / / /    License    \ \ \ \
#     Copyright © 2020-2021 Memex.Team
#     This code is covered by a Memex.Team Licensing policy (License.md)
#     Feel free to use a power of Memex.Team & Memex.Team trademarks!
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#      Language = ruby
#      Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######
# frozen_string_literal: true

module ReBro
  module API
    class Root < Grape::API
      include ReBro::Env::Mixin::Auth
      format :json

      rescue_from :all do |e|
        res = GrapeErrorHandler(e)
        ReBro::Events::App.info(res, :console)
        error!(res, 500)
      end

      helpers do
        def authorize(hIn)
          if @@authDB[:tokens].key?(hIn['HTTP_X_SESSION_TOKEN'])
            token = @@authDB[:tokens][hIn['HTTP_X_SESSION_TOKEN']]
            fingerprint = @@authDB[:fingerprints][token[:fingerprint]]
            userData = @@authDB[:users][fingerprint[:authType]][fingerprint[:id]]
            args = {
              authType: fingerprint[:authType],
              id: fingerprint[:id],
              joined: token[:joined],
              userData: userData
            }
            res = HTree.new(args)
          else
            res = false
          end

          res
        end

        def current_user
          @current_user ||= authorize(env)
        end

        def authenticate!
          if current_user
            res = current_user
          else
            res = false
            error!('401 Unauthorized', 401)
          end
          res
        end
      end

      mount ReBro::Env.api[:class]::Actions => ReBro::Env.api[:path]

      add_swagger_documentation format: :json,
                                info: {
                                  title: 'Metaframe API',
                                  description: 'Metaframe API',
                                  contact_name: 'Eugene Istomin',
                                  contact_email: 'e.istomin@memex.team',
                                  license: 'Memex.Team'
                                },
                                mount_path: "#{ReBro::Env.api[:path]}/oapi",
                                schemes: %w[http https]
    end
  end
end
