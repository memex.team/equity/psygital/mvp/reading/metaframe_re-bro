#######    ########    #######    ########    #######    #######    #######
#       / / / /    License    \ \ \ \
#     Copyright © 2020-2021 Memex.Team
#     This code is covered by a Memex.Team Licensing policy (License.md)
#     Feel free to use a power of Memex.Team & Memex.Team trademarks!
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#      Language = ruby
#      Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######
# frozen_string_literal: true

module ReBro
  module Microservices
    module Contextme
      module Reading
        module Metaframe
          module API
            class Actions < Grape::API
              include Grape::Extensions::HTree::ParamBuilder

              version 'v1', using: :header, vendor: 're-bro'
              format :json
              namespace '/actions/ctp' do
                post '/store' do
                  params._user = authenticate!

                  case params._ctp._general._mode.to_i
                  # onboarding
                  when 0
                    res = ReBro::Microservices::Contextme::Reading::Metaframe::CTP.store(params)
                  # fillthegap
                  when 1
                    res = {
                      "error": {
                        "msg": 'Кстати, в этой книге есть ответ на этот вопрос. Открывали 166 страницу?',
                        "caption": 'Go to Страница 166 :)'
                      }
                    }
                  # quiz
                  when 2
                    res = {
                        "question": {
                          "msg": 'Спринт может длиться дольше 4 недель?',
                          "caption": 'Кстати, про спринт',
                          "uuid": 'aabbccddeeff11223344556677889900'
                        }
                    }
                  # battle
                  when 3
                    res = {
                      "information": { "msg": 'Вы на верном пути!', "caption": 'Good luck' }
                    }
                  end
                  status 200

                  res
                end

                post '/feedback' do
                  ReBro::Events::App.info(params)
                  res = { result: 'OK' }
                  status 200
                  res[:result]
                end
              end
            end
          end
        end
      end
    end
  end
end
