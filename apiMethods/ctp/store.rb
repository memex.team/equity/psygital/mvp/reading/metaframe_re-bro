#######    ########    #######    ########    #######    #######    #######
#       / / / /    License    \ \ \ \
#     Copyright © 2020-2021 Memex.Team
#     This code is covered by a Memex.Team Licensing policy (License.md)
#     Feel free to use a power of Memex.Team & Memex.Team trademarks!
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#      Language = ruby
#      Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######
# frozen_string_literal: true

module ReBro
  module Microservices
    module Contextme
      module Reading
        module Metaframe
          module CTP
            def self.store(hIn)
              ReBro::Protocols::CTP::Neo4J.store(hIn)
            end
          end
        end
      end
    end
  end
end
