#######    ########    #######    ########    #######    #######    #######
#       / / / /    License    \ \ \ \
#     Copyright © 2020-2021 Memex.Team
#     This code is covered by a Memex.Team Licensing policy (License.md)
#     Feel free to use a power of Memex.Team & Memex.Team trademarks!
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#      Language = ruby
#      Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######
# frozen_string_literal: true

module ReBro
  module Microservices
    module Contextme
      module Reading
        module Metaframe
          module Models
            module GraphSchema
              class User
                include ActiveGraph::Node
                include ActiveGraph::Timestamps
                id_property :userId
                property :tsUpdate
                property :authType
                property :joined

                self.mapped_label_name = 'user'
              end

              class Cogtype
                include ActiveGraph::Node
                include ActiveGraph::Timestamps
                id_property :huuid
                property :tsUpdate
                property :name

                property :cogId

                self.mapped_label_name = 'cogType'
              end

              class Page
                include ActiveGraph::Node
                include ActiveGraph::Timestamps
                id_property :huuid
                property :tsUpdate
                property :page
                property :pages
                property :ratio

                self.mapped_label_name = 'pages'
              end

              class Focus
                include ActiveGraph::Node
                include ActiveGraph::Timestamps
                id_property :huuid
                property :tsUpdate
                property :type

                self.mapped_label_name = 'focus'
              end

              class Cog
                include ActiveGraph::Node
                include ActiveGraph::Timestamps

                id_property :huuid
                property :name
                property :tsUpdate

                property :text
                property :color
                property :coords

                has_one :out, :doc, type: :doc, model_class: :Doc
                has_one :out, :frame, type: :frame, model_class: :Frame

                self.mapped_label_name = 'cog'
              end

              class Doc
                include ActiveGraph::Node
                include ActiveGraph::Timestamps
                id_property :huuid
                property :tsUpdate
                property :name

                property :bytesize
                property :filename
                property :subject
                property :title

                has_many :in, :doc, type: false, model_class: false

                self.mapped_label_name = 'doc'
              end

              class Frame
                include ActiveGraph::Node
                include ActiveGraph::Timestamps
                id_property :huuid
                property :tsUpdate
                property :name

                has_many :in, :frame, type: false, model_class: false

                self.mapped_label_name = 'frame'
              end
            end

            def self.storeUser(hIn)
              data = {
                userId: hIn._user._id,
                authType: hIn._user._authType,
                joined: hIn._user._joined,
                tsUpdate: hIn._tsNow
              }
              ReBro::Microservices::Contextme::Reading::Metaframe::Models::GraphSchema::User.find_or_create_by!(data)
            end

            def self.typeMapping
              {
                '0' => :left,
                '1' => :up,
                '2' => :down,
                '3' => :right
              }
            end

            def self.storeDoc(hIn)
              data = {
                huuid: hIn._huuid,
                name: hIn._data._filename[0..10],
                tsUpdate: hIn._tsNow,
                bytesize: hIn._data._bytesize,
                filename: hIn._data._filename,
                subject: hIn._data._subject,
                title: hIn._data._title
              }
              ReBro::Microservices::Contextme::Reading::Metaframe::Models::GraphSchema::Doc.find_or_create_by!(data)
            end

            def self.storeFrame(hIn)
              data = {
                huuid: hIn._huuid,
                tsUpdate: hIn._tsNow,
                name: hIn._data._name
              }
              ReBro::Microservices::Contextme::Reading::Metaframe::Models::GraphSchema::Frame.find_or_create_by!(data)
            end

            def self.storePage(hIn)
              data = {
                huuid: hIn._huuid,
                tsUpdate: hIn._tsNow,
                page: hIn._data._page,
                pages: hIn._data._pages,
                ratio: hIn._data._ratio
              }
              ReBro::Microservices::Contextme::Reading::Metaframe::Models::GraphSchema::Page.find_or_create_by!(data)
            end

            def self.storeFocus(hIn)
              data = {
                huuid: hIn._huuid,
                tsUpdate: hIn._tsNow,
                type: typeMapping[hIn._data._type.to_s]
              }
              ReBro::Microservices::Contextme::Reading::Metaframe::Models::GraphSchema::Focus.find_or_create_by!(data)
            end

            def self.storeCogtype(hIn)
              data = {
                huuid: hIn._huuid,
                tsUpdate: hIn._tsNow,
                name: hIn._data._name,
                cogId: hIn._data._id
              }
              ReBro::Microservices::Contextme::Reading::Metaframe::Models::GraphSchema::Cogtype.find_or_create_by!(data)
            end

            def self.storeCog(hIn)
              data = {
                huuid: hIn._huuid,
                tsUpdate: hIn._tsNow,
                name: hIn._data._text[0..10],
                text: hIn._data._text,
                color: hIn._data._color,
                coords: hIn._data._coords
              }
              ReBro::Microservices::Contextme::Reading::Metaframe::Models::GraphSchema::Cog.find_or_create_by!(data)
            end
          end
        end
      end
    end
  end
end
