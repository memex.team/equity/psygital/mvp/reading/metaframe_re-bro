#######    ########    #######    ########    #######    #######    #######
#       / / / /    License    \ \ \ \
#     Copyright © 2020-2021 Memex.Team
#     This code is covered by a Memex.Team Licensing policy (License.md)
#     Feel free to use a power of Memex.Team & Memex.Team trademarks!
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#      Language = ruby
#      Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######
# frozen_string_literal: true

module ReBro
  module Microservices
    module Contextme
      module Reading
        module Metaframe
          module Models
            module GraphSchema
              class Rel_cog2doc
                include ActiveGraph::Relationship
                include ActiveGraph::Timestamps
                # property :huuid
                # property :tsUpdate
                from_class :any
                to_class   :any

                type 'cog2doc'
              end

              class Rel_cog2focus
                include ActiveGraph::Relationship
                include ActiveGraph::Timestamps
                # property :huuid
                # property :tsUpdate
                from_class :any
                to_class   :any

                type 'cog2focus'
              end
            end
          end
        end
      end
    end
  end
end
